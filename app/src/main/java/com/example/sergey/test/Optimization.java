package com.example.sergey.test;

public class Optimization {
    public static String minMoves(Coordinates greenHorse, Coordinates blueHorse) {
        int distanceHorizontally = greenHorse.distanceHorizontally(blueHorse);
        int distanceVertically = greenHorse.distanceVertically(blueHorse);
        if ((distanceHorizontally + distanceVertically) % 2 != 0)
            return "IMPOSSIBLE";
        if (distanceHorizontally < 5 && distanceVertically < 5) {
            if (distanceHorizontally == 2 && distanceVertically == 2)
                return "2";
            if (distanceHorizontally == 4 && distanceVertically == 4)
                return "2";
            return "1";
        }
        if (distanceHorizontally == 7 && distanceVertically == 7)
            return "3";
        return "2";
    }
}
