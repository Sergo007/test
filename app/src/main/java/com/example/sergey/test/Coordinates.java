package com.example.sergey.test;


public class Coordinates {
    public int vertically;
    public int horizontal;

    Coordinates(int horizontal, int vertically){
        this.vertically = vertically;
        this.horizontal = horizontal;
    }

    Coordinates(String coordinates) {
        this.horizontal = Converter.chessCharToInt(coordinates.charAt(0));
        this.vertically = Integer.parseInt(String.valueOf(coordinates.charAt(1)));
    }

    Coordinates(){
        this.vertically = 0;
        this.horizontal = 0;
    }

    Coordinates(Coordinates coordinates){
        this.vertically = coordinates.vertically;
        this.horizontal = coordinates.horizontal;
    }

    public int distanceHorizontally(Coordinates coordinates){
        return Math.abs(coordinates.horizontal-horizontal);
    }

    public int distanceVertically(Coordinates coordinates){
        return Math.abs(coordinates.vertically - vertically);
    }

    public boolean equals(Coordinates coordinates){
        return (vertically==coordinates.vertically)&&(horizontal==coordinates.horizontal);
    }
}
