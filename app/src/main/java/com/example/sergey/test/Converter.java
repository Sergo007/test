package com.example.sergey.test;

/**
 * Created by Sergey on 11.03.2016.
 */
public class Converter {
    public static int chessCharToInt(char charChess) {
        int s = 0;
        switch (charChess) {
            case 'a':
                s = 1;
                break;
            case 'b':
                s = 2;
                break;
            case 'c':
                s = 3;
                break;
            case 'd':
                s = 4;
                break;
            case 'e':
                s = 5;
                break;
            case 'f':
                s = 6;
                break;
            case 'g':
                s = 7;
                break;
            case 'h':
                s = 8;
                break;
        }
        return s;
    }
}
