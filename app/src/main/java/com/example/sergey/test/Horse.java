package com.example.sergey.test;


public class Horse {
    private Coordinates coordinats = new Coordinates();

    Horse(String coordinates) {
        coordinats.horizontal = Converter.chessCharToInt(coordinates.charAt(0));
        coordinats.vertically = Integer.parseInt(String.valueOf(coordinates.charAt(1)));
    }

    Horse(int horizontal, int vertically) {
        coordinats.horizontal = horizontal;
        coordinats.vertically = vertically;
    }

    Horse(Coordinates coordinats) {
        this.coordinats = new Coordinates(coordinats);
    }

    public Coordinates getCoordinates() {
        return coordinats;
    }


    public Coordinates[] allRoads() {
        Coordinates[] coordinates = new Coordinates[8];
        coordinates[0] = new Coordinates(coordinats.horizontal - 1, coordinats.vertically - 3);
        coordinates[1] = new Coordinates(coordinats.horizontal + 1, coordinats.vertically - 3);
        coordinates[2] = new Coordinates(coordinats.horizontal + 2, coordinats.vertically - 1);
        coordinates[3] = new Coordinates(coordinats.horizontal + 2, coordinats.vertically + 1);
        coordinates[4] = new Coordinates(coordinats.horizontal + 1, coordinats.vertically + 2);
        coordinates[5] = new Coordinates(coordinats.horizontal - 1, coordinats.vertically + 2);
        coordinates[6] = new Coordinates(coordinats.horizontal - 3, coordinats.vertically + 1);
        coordinates[7] = new Coordinates(coordinats.horizontal - 2, coordinats.vertically - 1);
        return coordinates;
    }

    boolean isCanMove(Coordinates coordinates) {
        Coordinates[] allRoads = allRoads();
        for (int i = 0; i < 8; i++) {
            if ((allRoads[i].horizontal >= 0 && allRoads[i].vertically >= 0)&&(allRoads[i].horizontal <= 8 && allRoads[i].vertically <= 8))
                if (allRoads[i].equals(coordinates))
                    return true;
        }
        return false;
    }

}
