package com.example.sergey.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText blueHorse;
    EditText greenHorse;
    TextView moves;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        blueHorse = (EditText) findViewById(R.id.blue_horse);
        greenHorse = (EditText) findViewById(R.id.green_horse);
        moves = (TextView) findViewById(R.id.min_moves);
    }

    public void onClick(View view){
        Log.d("aaaa","greenHorse  " + greenHorse.getText().toString());
        Log.d("aaaa","blueHorse  " + blueHorse.getText().toString());
        moves.setText(Optimization.minMoves(new Coordinates(greenHorse.getText().toString()), new Coordinates(blueHorse.getText().toString())));
    }
}
